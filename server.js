// ------ Require the packages
var express = require('express');
var mongoose = require('mongoose');     
var morgan = require('morgan');             
var bodyParser = require('body-parser');   
var methodOverride = require('method-override');

// ------ Configure the application
var app = express();                       

mongoose.connect('mongodb://localhost:27017/trout');     

app.use(express.static(__dirname + '/public'));                 
app.use(morgan('dev'));                                         
app.use(bodyParser.urlencoded({'extended':'true'}));            
app.use(bodyParser.json());                                     
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
app.use(methodOverride());

// ------ Define the models
var Task = mongoose.model('Task', {
    text : String
});

// ------ Build the routes

// get all tasks
app.get('/api/tasks', function(req, res) {
    Task.find(function(err, tasks) {
        if (err){
            res.send(err);
        } else {
            res.json(tasks);
        }
    });
});

// create task and send back all tasks after creation
app.post('/api/tasks', function(req, res) {
    Task.create({
        text : req.body.text,
        done : false
    }, function(err, task) {
        if (err){
            res.send(err);
        }
        Task.find(function(err, tasks) {
            if (err){
                res.send(err);
            } else {
                res.json(tasks);
            }
        });
    });

});

// delete a single task
app.delete('/api/tasks/:task_id', function(req, res) {
    Task.remove({
        _id : req.params.task_id
    }, function(err, task) {
        if (err) {
            res.send(err);
        }
        Task.find(function(err, tasks) {
            if (err) {
                res.send(err);
            } else {
                res.json(tasks);
            }
        });
    });
});

//  ------ Serve the public
app.listen(3000);
console.log("Running on port 3000");